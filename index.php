<?php
/**
 * Created by PhpStorm.
 * User: louis-cuny
 * Date: 06/12/2016
 * Time: 11:51
 */


require 'vendor/autoload.php';
use giftbox\controllers\CatalogueControllers as CatalogueControllers;
use giftbox\controllers\CategorieControllers as CategorieControllers;
use giftbox\controllers\CoffretControllers as CoffretControllers;
use Slim\Slim;
//use giftbox\models\Prestation as Prestation;
session_start();
\conf\Eloquent::init('src/conf/conf.ini');

$app = new \Slim\Slim();




$app->get('/', function () {
    $html = <<<END
<!DOCTYPE html>
<html lang="fr">
<head> 
    <title>GiftBox</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="icon" type="image/png" href="img/cadeaux.jpg">
</head>

<body class="code-snippets-visible">
    <div class="container">
        <section class="header">
            <a href="/giftbox/"><img class="logo" src="img/logo.png" alt="logo"/></a>
        </section>
        <div class="navbar-spacer"></div>
        <nav class="navbar">
          <div class="container">
            <ul class="navbar-list">
                <li class="navbar-item"><a class="navbar-link" href="/giftbox/">Accueil</a></li>
                <li class="navbar-item"><a class="navbar-link" href="/giftbox/prestations/">Prestations</a></li>
                <li class="navbar-item"><a class="navbar-link" href="/giftbox/categories/">Categories</a></li>
                <li class="navbar-item"><a class="navbar-link" href="/giftbox/coffret/">Coffret</a></li>
            </ul>
        </div>
    </nav>
</body>
</html>
END;
    echo $html;
});

$app->get('/prestations/', function () {
   // $app = \Slim\Slim::getInstance() ;
    (new CatalogueControllers)->listePrestas();
});


$app->get('/prestations/:id', function ($id) {
    (new CatalogueControllers)->prestation($id);
})->name('prestation');

$app->get('/categories/', function () {
    (new CatalogueControllers)->ListeCategorie();
});

$app->get('/categories/:id', function ($id) {
    (new CatalogueControllers)->categorie($id);
})->name('categorie');


$app->get('/coffret/', function(){
    (new CoffretControllers())->afficherCoffret();
});


$app->get('/coffret/:id', function($id) {
    (new CoffretControllers())->ajouterAuCoffret($id);
});

$app->get('/paiement/', function(){
(new CoffretControllers())->payerCoffret();

});

$app->run();

