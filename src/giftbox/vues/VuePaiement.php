<?php
/**
 * Created by PhpStorm.
 * User: Alex Curman
 * Date: 18/01/2017
 * Time: 20:21
 */

namespace giftbox\vues;


class VuePaiement
{
    public $tabUrl;

    public function __construct()
    {
        $tabUrl = array();
    }

    public function CreationUrl($car){
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy0123456789";
        srand((double)microtime()*time());

        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }

        $tabUrl = [$string];
        return "localhost/giftbox/" . $string;
    }

    public function demanderInfo(){
        $html = "";
        $html.="
        <div class=\"paiement\">
        <form id='5' method='get'
        <fieldset>
        <input type='text' name='carte' placeholder='numero de carte' required>
        <input type='text' name='cryptogramme' placeholder='cryptogramme' required>  
        <input type='text' name='peremption carte' placeholder='MM/AA' required>
        </fieldset>
        <button type='submit'>valider</button>
        </form>
        </div>
        ";
        $this->render($html);
    }

    public function render($content){
        $html = <<<END
    <!DOCTYPE html>
<html lang="fr">
<head> 
    <title>GiftBox</title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="icon" type="image/png" href="img/cadeaux.jpg">
</head>

<body class="code-snippets-visible">
    <div class="container">
        <section class="header">
            <a href="/giftbox/"><img class="logo" src="../img/logo.png" alt="logo"/></a>
        </section>
        <div class="navbar-spacer"></div>
        <nav class="navbar">
            <div class="container">
                <ul class="navbar-list">
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/">Accueil</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/prestations/">Prestations</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/categories/">Categories</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/coffret/">Coffret</a></li>
                </ul>
            </div>
        </nav>
        $content
    </div>
</body>
</html>
END;
        echo $html;
    }

}