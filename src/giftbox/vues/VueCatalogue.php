<?php
/**
 * Created by PhpStorm.
 * User: Alex Curman
 * Date: 14/12/2016
 * Time: 16:32
 */

namespace giftbox\vues;
use giftbox\models\Prestation as Prestation;
use giftbox\models\Categorie as Categorie;

class VueCatalogue
{

    public function __construct(/*Array $tab*/){
    }

    public function listePrestation(){

        $img = null;
        $id = null;
        $c = null;
        $colone = 0;
        $prestations = "
               <a class='button' href='/giftbox/prestations/?tri=croissant'> prix croissant </a>
               <a class='button' href='/giftbox/prestations/?tri=decroissant'> prix decroissant </a>         
                        ";
        if(isset($_GET['tri'])) {
            if(($_GET['tri'] == 'croissant')) {
                $liste = Prestation::OrderBy('prix', 'ASC')->get();
            }
            if(($_GET['tri'] == 'decroissant')) {
                $liste = Prestation::OrderBy('prix', 'DESC')->get();
            }
        } else {
            $liste = Prestation::OrderBy('nom')->get();
        }

        foreach ($liste as $p) {
            $img = $p->img;
            $colone++;
            $id = $p->cat_id;
            $c = Categorie::where('id', '=', $id)->first();

            if ($colone == 1) {
                $prestations .=
                    "
                <div class=\"row docs-section\">
                ";
            }

            $prestations .=
                "    <div class=\"presta four columns\" id=\"$p->id\">
                               <a href=$p->id>

                                    <h4>$p->nom</h4>
                                    <p class=\"prix\"> $p->prix €</p>
                                    <img class=\"value-img image\" src=\"../img/$img\" alt=\"$img\">
                                </a>
                                
                                <a href=/giftbox/categories/$c->id>
                                    <h5>$c->nom</h5>
                               </a>
                             </div>
                             ";
            if ($colone == 3) {
                $prestations .=
                    "
                </div>
                ";
                $colone = 0;
            }
        }

        $this->render($prestations);
    }



    public function prestation($id){

        /*if (!isset($_SESSION['coffret'])){
           $_SESSION['coffret'] = array();
        }*/
        if ($id > 0) {
            $p = Prestation::where('id', '=', $id)->first();
            $img = $p->img;
            $prestation = "<div class=\"presta four columns\" id=\"$id\">
                                <h3 > $p->nom</h3 >
                                <p class=\"prix\"> $p->prix € </p>
                                <p><img src=\"../img/$img\" alt=\"$img\"></p>
                                <p>$p->descr</p>
                                   <a class='button' href='/giftbox/coffret/$id'>Ajouter au coffret</a>
                           </div>
                           
            ";
            $this->render($prestation);
        }
    }
/*<input type='text' value=\"$id\" name=\"idpresta\" hidden>*/


    public function ListeCategorie(){

        $liste = Categorie::OrderBy('nom')->get();
        $categories =
            "<div class=\"container\">";
        foreach ($liste as $c) {
            $categories .= "<div class=\"cat\" id=\"$c->id\">
                                <a href =$c->id>
                                <h3>$c->nom</h3>      
                                </a>
                            </div><br/>";
        }
        $categories .= "</div>"
        ;
        $this->render($categories);
    }

    public function categorie($id){

        $img = null;
        $c = Categorie::where('id', '=', $id)->first();
        $colone = 0;
        $prestations = "<div class=\"container\">

                           <a class='button' href='/giftbox/categories/$id?tri=croissant'> prix croissant </a>
                           <a class='button' href='/giftbox/categories/$id?tri=decroissant'> prix decroissant </a>
                        </div>
                        
            <h1>$c->nom</h1>";

        if(isset($_GET['tri'])) {
            if(($_GET['tri'] == 'croissant')) {
                $liste = Prestation::OrderBy('prix', 'ASC')->where('cat_id', 'like', $id)->get();
            }
            if(($_GET['tri'] == 'decroissant')) {
                $liste = Prestation::OrderBy('prix', 'DESC')->where('cat_id', 'like', $id)->get();
            }
        } else {
            $liste = Prestation::OrderBy('nom')->where('cat_id', 'like', $id)->get();
        }

        foreach ($liste as $p) {
            $img = $p->img;
            $colone++;
            $id = $p->cat_id;
            $c = Categorie::where('id', '=', $id)->first();

            if ($colone == 1) {
                $prestations .=
                    "
                <div class=\"row docs-section\">
                ";
            }

            $prestations .=
                "    <div class=\"presta four columns\" id=\"$p->id\">
                               <a href=/giftbox/prestations/$p->id>
                                    <h4>$p->nom</h4>
                                    <p class=\"prix\"> $p->prix €</p>
                                    <img class=\"value-img\" src=\"../img/$img\" alt=\"$img\" width=\"100\" height=\"100\">
                                </a>                               
                             </div>
                             ";
            if ($colone == 3) {
                $prestations .=
                    "
                </div>
                ";
                $colone = 0;
            }
        }
        if ($colone != 1) {
            $prestations .= "</div>";
        }
        $this->render($prestations);
    }

    private function render($content){
        $html = <<<END
    <!DOCTYPE html>
<html lang="fr">
<head> 
    <title>GiftBox</title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="icon" type="image/png" href="img/cadeaux.jpg">
</head>

<body class="code-snippets-visible">
    <div class="container">
        <section class="header">
            <a href="/giftbox/"><img class="logo" src="../img/logo.png" alt="logo"/></a>
        </section>
        <div class="navbar-spacer"></div>
        <nav class="navbar">
            <div class="container">
                <ul class="navbar-list">
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/">Accueil</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/prestations/">Prestations</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/categories/">Categories</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/coffret/">Coffret</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="docs-section" id="body-prestations">
        <div class="container">
        $content
        </div>
    </div>
</body>
</html>
END;
        echo $html;
    }

}