<?php
/**
 * Created by PhpStorm.
 * User: Alex Curman
 * Date: 12/01/2017
 * Time: 11:10
 */

namespace giftbox\vues;


class VueCoffret
{

    public function __construct()
    {
    }



    public function coffret()
    {
        $achats = "a";
        if (isset($_SESSION['coffret'])) {
            $liste = $_SESSION['coffret'];

            $img = null;

            foreach ($liste as $p) {
                $achats .= "<div class=\"Coffret\">
                                <h3>$p</h3>
                                </div><br/>";

            }
        } else {
                $achats = "<div class=\"Coffret\">
                                <h3> Votre coffret est vide !!!</h3>      
                                </div>";
            }
            $this->render($achats);
        }


    public function ajouterPresta($num){
    if (!isset($_SESSION['coffret'])){
       $_SESSION['coffret'] = array();
    }
    if ($_SESSION['coffret'] == null) {
        $_SESSION['coffret'][$num] = 1;
    } else {
        $_SESSION['coffret'][$num]++;
    }


        $a= "<p>Article ajouté au coffret avec succes !</p><br/>
                <a class='button' href='/giftbox/coffret'>Voir le coffret</a>";
        $this->render($a);
    }


        public function render($content){

            $html = <<<END
    <!DOCTYPE html>
<html lang="fr">
<head> 
    <title>GiftBox</title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/custom.css">
    <link rel="icon" type="image/png" href="img/cadeaux.jpg">
</head>

<body class="code-snippets-visible">
    <div class="container">
        <section class="header">
            <a href="/giftbox/"><img class="logo" src="../img/logo.png" alt="logo"/></a>
        </section>
        <div class="navbar-spacer"></div>
        <nav class="navbar">
            <div class="container">
                <ul class="navbar-list">
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/">Accueil</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/prestations/">Prestations</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/categories/">Categories</a></li>
                    <li class="navbar-item"><a class="navbar-link" href="/giftbox/coffret/">Coffret</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="docs-section" id="body-prestations">
    <div class="container">
        $content
        <a class="button" href="/giftbox/paiement/">Payer</a>
    </div>
    </div>
</body>
</html>
END;

        echo $html;
    }

}