<?php

/**
 * Created by PhpStorm.
 * User: louis-cuny
 * Date: 13/12/2016
 * Time: 11:33
 */

namespace giftbox\controllers;
use giftbox\vues\VueCatalogue as VueCatalogue;

class CatalogueControllers
{
    //pour les prestations

    public function listePrestas(){
        (new VueCatalogue())->listePrestation();
    }

    public function prestation($id){
        (new VueCatalogue())->prestation($id);
    }

    public function PrestCategorie($categorie){
        (new VueCatalogue())->PrestCategorie($categorie);
    }

    //pour les categories

    public function ListeCategorie(){
        (new VueCatalogue())->ListeCategorie();
    }

    public function categorie($id){
        (new VueCatalogue())->categorie($id);
    }

}