<?php
/**
 * Created by PhpStorm.
 * User: Alex Curman
 * Date: 12/01/2017
 * Time: 11:09
 */

namespace giftbox\controllers;


use giftbox\vues\VueCoffret;
use giftbox\vues\VuePaiement;

class CoffretControllers
{

    public function afficherCoffret(){
        (new VueCoffret())->coffret();
    }


    public function ajouterAuCoffret($num)
    {
        (new VueCoffret())->ajouterPresta($num);
    }

    public function payerCoffret(){
        (new VuePaiement())->demanderInfo();

    }

    /*public function ajouterAuCoffret($id){
        $this->creationCoffret();
        $p= Prestation::where('id', $id)->first();
        $_SESSION['box'][]=$p;
        $_SESSION['num']++;
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->request->getRootUri().'/coffret/');

    }

    private function creationCoffret(){
        if(!isset($_SESSION['box'])){
            $_SESSION['box']= array();
            $_SESSION['num']= 0;
        }
    }*/
}