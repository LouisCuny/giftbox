<?php
/**
 * Created by PhpStorm.
 * User: louis-cuny
 * Date: 06/12/2016
 * Time: 11:34
 */

namespace giftbox\models;
use Illuminate\Database\Eloquent\Model;

class Prestation extends Model {
    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

