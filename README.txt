Projet Giftbox développé par Siméon Gautherot, Louis Cuny et Alex Curman élèves de S3A 2016

Louis s'est occupé du HTML et d'introduire le framework css

Fonctionnalités du projet :

1 Afficher la liste des prestations
complète
Siméon Gautherot - Cuny Louis

2 Afficher le détail d'une prestation
complète
Siméon Gautherot - Cuny Louis

3 liste de prestations par catégories
complète
Siméon Gautherot - Cuny Louis

4 liste de catégories
complète
Siméon Gautherot - Cuny Louis

5 Tri par prix
complète
Siméon gautherot

6 Notation des prestations
non abordée

7 Notes moyennes
non abordée

8 Page d'accueil
non abordée

9 ajout de prestations dans le coffret
partielle
Alex Curman - Cuny Louis

10 affichage d'un coffret
partielle
Alex Curman - Cuny Louis

11 validation d'un coffret
partielle
Alex Curman - Cuny Louis

12 paiement classique
partielle
Alex Curman

13 ajout d'un mot de passe de gestion du coffret
non abordée

14 modification d'un coffret
non abordée

15 création d'une cagnotte
non abordée

16 participer à la cagnotte
non abordée

17 cloturer la cagnotte
non abordée

18 génération de l'URL à offrir
partielle
Siméon Gautherot

19 ouverture du cadeau
non abordée

20 suivi du coffret cadeau
non abordée

21 ajout de prestations
non abordée

22 suppression de prestations
non abordée

23 desactivation de prestations
non abordée

24 authentification des gestionnaires
non abordée